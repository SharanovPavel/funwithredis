﻿using StackExchange.Redis;
using StackExchange.Redis.Profiling;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Services.Redis
{
    public class DisconnectedConnectionMultiplexer : IConnectionMultiplexer
    {
        public string ClientName => throw GetDisconnectedException();

        public string Configuration => throw GetDisconnectedException();

        public int TimeoutMilliseconds => throw GetDisconnectedException();

        public long OperationCount => throw GetDisconnectedException();

        public bool PreserveAsyncOrder { get => throw GetDisconnectedException(); set => throw GetDisconnectedException(); }

        public bool IsConnected => false;

        public bool IsConnecting => false;

        public bool IncludeDetailInExceptions { get => throw GetDisconnectedException(); set => throw GetDisconnectedException(); }
        public int StormLogThreshold { get => throw GetDisconnectedException(); set => throw GetDisconnectedException(); }

        public event EventHandler<RedisErrorEventArgs> ErrorMessage;
        public event EventHandler<ConnectionFailedEventArgs> ConnectionFailed;
        public event EventHandler<InternalErrorEventArgs> InternalError;
        public event EventHandler<ConnectionFailedEventArgs> ConnectionRestored;
        public event EventHandler<EndPointEventArgs> ConfigurationChanged;
        public event EventHandler<EndPointEventArgs> ConfigurationChangedBroadcast;
        public event EventHandler<HashSlotMovedEventArgs> HashSlotMoved;

        public void Close(bool allowCommandsToComplete = true)
        {
            throw GetDisconnectedException();
        }

        public Task CloseAsync(bool allowCommandsToComplete = true)
        {
            throw GetDisconnectedException();
        }

        public bool Configure(TextWriter log = null)
        {
            throw GetDisconnectedException();
        }

        public Task<bool> ConfigureAsync(TextWriter log = null)
        {
            throw GetDisconnectedException();
        }

        public void Dispose()
        {
            throw GetDisconnectedException();
        }

        public void ExportConfiguration(Stream destination, ExportOptions options = (ExportOptions)(-1))
        {
            throw GetDisconnectedException();
        }

        public ServerCounters GetCounters()
        {
            throw GetDisconnectedException();
        }

        public IDatabase GetDatabase(int db = -1, object asyncState = null)
        {
            throw GetDisconnectedException();
        }

        public EndPoint[] GetEndPoints(bool configuredOnly = false)
        {
            throw GetDisconnectedException();
        }

        public int GetHashSlot(RedisKey key)
        {
            throw GetDisconnectedException();
        }

        public IServer GetServer(string host, int port, object asyncState = null)
        {
            throw GetDisconnectedException();
        }

        public IServer GetServer(string hostAndPort, object asyncState = null)
        {
            throw GetDisconnectedException();
        }

        public IServer GetServer(IPAddress host, int port)
        {
            throw GetDisconnectedException();
        }

        public IServer GetServer(EndPoint endpoint, object asyncState = null)
        {
            throw GetDisconnectedException();
        }

        public string GetStatus()
        {
            throw GetDisconnectedException();
        }

        public void GetStatus(TextWriter log)
        {
            throw GetDisconnectedException();
        }

        public string GetStormLog()
        {
            throw GetDisconnectedException();
        }

        public ISubscriber GetSubscriber(object asyncState = null)
        {
            throw GetDisconnectedException();
        }

        public int HashSlot(RedisKey key)
        {
            throw GetDisconnectedException();
        }

        public long PublishReconfigure(CommandFlags flags = CommandFlags.None)
        {
            throw GetDisconnectedException();
        }

        public Task<long> PublishReconfigureAsync(CommandFlags flags = CommandFlags.None)
        {
            throw GetDisconnectedException();
        }

        public void RegisterProfiler(Func<ProfilingSession> profilingSessionProvider)
        {
            throw GetDisconnectedException();
        }

        public void ResetStormLog()
        {
            throw GetDisconnectedException();
        }

        public void Wait(Task task)
        {
            throw GetDisconnectedException();
        }

        public T Wait<T>(Task<T> task)
        {
            throw GetDisconnectedException();
        }

        public void WaitAll(params Task[] tasks)
        {
            throw GetDisconnectedException();
        }

        private static RedisConnectionException GetDisconnectedException()
            => new RedisConnectionException(ConnectionFailureType.UnableToConnect, "Disconnected");
    }
}
