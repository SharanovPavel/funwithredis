﻿using Services.Abstractions.Redis.Interactors;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Redis.Interactors
{
    class SubscribeInteractor : Interactor, ISubscribeInteractor
    {
        public SubscribeInteractor(IConnectionMultiplexer redis)
            : base(redis) { }

        public long Publish(string channel, string value)
            => _redis.GetSubscriber().Publish(channel, value);
    }
}
