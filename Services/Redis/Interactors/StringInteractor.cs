﻿using System;
using System.Collections.Generic;
using System.Text;
using Services.Abstractions.Redis.Interactors;
using StackExchange.Redis;

namespace Services.Redis.Interactors
{
    class StringInteractor : Interactor, IStringInteractor
    {
        public StringInteractor(IConnectionMultiplexer redis)
            : base(redis) { }

        public string Get(int databaseId, string key)
            => GetDatabase(databaseId).StringGet(key);

        public long Increment(int databaseId, string key)
            => IncrementBy(databaseId, key, 1);

        public long IncrementBy(int databaseId, string key, long value)
            => GetDatabase(databaseId).StringIncrement(key, value);

        public double IncrementBy(int databaseId, string key, double value)
            => GetDatabase(databaseId).StringIncrement(key, value);

        public bool Set(int databaseId, string key, string value)
            => GetDatabase(databaseId).StringSet(key, value);
    }
}
