﻿using StackExchange.Redis;
using System;

namespace Services.Redis.Interactors
{
    internal abstract class Interactor
    {
        protected readonly IConnectionMultiplexer _redis;

        public Interactor(IConnectionMultiplexer redis)
        {
            _redis = redis ?? throw new ArgumentNullException(nameof(redis));
        }

        protected IDatabase GetDatabase(int id)
            => _redis.GetDatabase(id);
    }
}
