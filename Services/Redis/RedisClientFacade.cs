﻿using Services.Abstractions;
using Services.Abstractions.Redis;
using Services.Abstractions.Redis.Interactors;
using Services.Redis.Interactors;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Redis
{
    public class RedisClientFacade : IRedisClient
    {
        public RedisClientFacade(IConnectionMultiplexer redis)
        {
            if (redis == null) throw new ArgumentNullException(nameof(redis));

            String = new StringInteractor(redis);

            Subscriber = new SubscribeInteractor(redis);
        }

        public IStringInteractor String { get; }

        public ISubscribeInteractor Subscriber { get; }
    }
}
