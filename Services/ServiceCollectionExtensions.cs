﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services.Abstractions.Redis;
using Services.Redis;
using StackExchange.Redis;
using System;

namespace Services
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRedisClient(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IConnectionMultiplexer>(provider => ConnectionMultiplexer.Connect(configuration["Host"], Console.Out));

            services.AddScoped<IRedisClient, RedisClientFacade>();

            return services;
        }
    }
}
