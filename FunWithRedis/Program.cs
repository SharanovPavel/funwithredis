﻿using StackExchange.Redis;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FunWithRedis
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var redis = ConnectionMultiplexer.Connect("192.168.56.102:7001"))
            {
                var subscriber = redis.GetSubscriber();

                var channel = "messages";

                var subsribeTask = subscriber.SubscribeAsync(channel, HandleMessage);

                var cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(20));

                var publishTask = StartPublishAsync(subscriber, channel, cancellationTokenSource.Token);

                publishTask.ContinueWith(_ =>
                {
                    Console.WriteLine("That's all, folks!");
                    Console.ReadKey();
                }).Wait();
            }

        }

        private static void HandleMessage(RedisChannel channel, RedisValue message)
        {
            Console.WriteLine(message);
        }

        private static Task StartPublishAsync(ISubscriber subscriber, string channel, CancellationToken token)
        {
            return Task.Run(() =>
            {
                while (!token.IsCancellationRequested)
                {
                    subscriber.Publish(channel, "Hello World!");
                    Thread.Sleep(1000);
                }
            });
        }
    }
}
