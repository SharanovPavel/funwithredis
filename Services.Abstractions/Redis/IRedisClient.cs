﻿using Services.Abstractions.Redis.Interactors;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Abstractions.Redis
{
    public interface IRedisClient
    {
        IStringInteractor String { get; }

        ISubscribeInteractor Subscriber { get; }
    }
}
