﻿namespace Services.Abstractions.Redis.Interactors
{
    public interface IStringInteractor
    {
        string Get(int databaseId, string key);

        bool Set(int databaseId, string key, string value);

        long Increment(int databaseId, string key);

        long IncrementBy(int databaseId, string key, long value);

        double IncrementBy(int databaseId, string key, double value);
    }
}
