﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Abstractions.Redis.Interactors
{
    public interface ISubscribeInteractor
    {
        long Publish(string channel, string value);
    }
}
