﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;

namespace WebApi
{
    public class Program
    {
        private static IConnectionMultiplexer Redis;

        public static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("redis.json")
                .Build();

            var redisConfiguration = configuration.GetSection("Redis");

            try
            {
                Redis = ConnectionMultiplexer.Connect(redisConfiguration["Host"], Console.Out);

                RunRedisListeners(Redis, redisConfiguration);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            CreateWebHostBuilder(args, configuration).Build().Run();
        }

        private static void RunRedisListeners(IConnectionMultiplexer redis, IConfiguration configuration)
        {
            redis.GetSubscriber().Subscribe(configuration["Channel"], (channel, value) =>
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(new string('-', 40));
                Console.WriteLine($"[{channel}] { value }");
                Console.WriteLine(new string('-', 40));
                Console.ResetColor();
            });
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args, IConfigurationRoot configuration) =>
            WebHost.CreateDefaultBuilder(args)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseConfiguration(configuration)
                .UseKestrel()
                .UseStartup<Startup>();
    }
}
