﻿using Microsoft.Extensions.Configuration;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Configuration;
using Nancy.Swagger.Services;
using Nancy.TinyIoc;
using Services.Abstractions.Redis;
using Services.Redis;
using StackExchange.Redis;
using System;
using WebApi.Swagger;

namespace WebApi.Nancy
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        private static IConnectionMultiplexer Redis;

        private readonly IConfiguration _configuration;

        public Bootstrapper()
        {
            _configuration = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json")
                .AddJsonFile($"redis.json")
                .AddJsonFile("swagger.config.json")
                .AddEnvironmentVariables()
                .Build();

            SwaggerConfigurator.Configure(_configuration.GetSection("Swagger"));
        }

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            this.Conventions.ViewLocationConventions.Add(
                (viewName, model, context) =>
                    string.Concat(_configuration["Nancy:Views:Path"], context.ModuleName, '/', viewName));

            base.ApplicationStartup(container, pipelines);
        }

        protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            pipelines.OnError.AddItemToEndOfPipeline((errorContext, exception) =>
            {
                return new Response().WithStatusCode(HttpStatusCode.InternalServerError);
            });

            base.RequestStartup(container, pipelines, context);
        }

        public override void Configure(INancyEnvironment environment)
        {
            environment.AddValue("Redis:Prefix", _configuration["Redis:Prefix"]);
            base.Configure(environment);
        }

        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            RegisterRedisApplication(container);
        }

        protected override void ConfigureRequestContainer(TinyIoCContainer container, NancyContext context)
        {
            RegisterRedisPerRequest(container);
        }

        private void RegisterRedisPerRequest(TinyIoCContainer container)
        {
            container.Register<IRedisClient, RedisClientFacade>();
        }

        private void RegisterRedisApplication(TinyIoCContainer container)
        {
            container.Register((_, __) =>
            {
                try
                {
                    if(Redis == null)
                        Redis = ConnectionMultiplexer.Connect(_configuration["Redis:Host"], Console.Out);

                    return Redis;
                }
                catch(RedisConnectionException e)
                {
                    Console.WriteLine(e);
                    return new DisconnectedConnectionMultiplexer();
                }
            });
        }
    }
}
