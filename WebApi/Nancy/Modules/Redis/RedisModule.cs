﻿using Nancy;
using Nancy.Configuration;
using Services.Abstractions.Redis;

namespace WebApi.Nancy.Modules.Redis
{
    public abstract class RedisModule : NancyModule
    {
        protected string Key => Request.Query["key"];

        protected string Value => Request.Query["value"];

        protected string Path { get; }
        
        public RedisModule(IRedisClient client, INancyEnvironment environment)
        {
            var prefix = FormatPrefix(environment["Redis:Prefix"].ToString());

            Path = FormatPath(prefix, GetModulePath());
        }

        protected abstract string GetModulePath();

        private string FormatPath(string prefix, string path)
        {
            return (prefix + path).Replace("//", "/");
        }

        private string FormatPrefix(string prefix)
        {
            var formattedPrefix = prefix;

            if (!formattedPrefix.StartsWith('/'))
                formattedPrefix = '/' + formattedPrefix;

            if (!formattedPrefix.EndsWith('/'))
                formattedPrefix = formattedPrefix + '/';

            return formattedPrefix;
        }
    }
}
