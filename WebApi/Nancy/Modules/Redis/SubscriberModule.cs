﻿using Nancy.Configuration;
using Services.Abstractions.Redis;
using System;

namespace WebApi.Nancy.Modules.Redis
{
    public class SubscriberModule : RedisModule
    {
        private string Channel => Request.Query["channel"];

        public SubscriberModule(IRedisClient client, INancyEnvironment environment) 
            : base(client, environment)
        {
            RoutePublish(client, Path + "publish", "Publish");
        }

        private void RoutePublish(IRedisClient client, string path, string name)
        {
            Func<dynamic, object> publish = args => client.Subscriber.Publish(Channel, Value);

            Post(path, publish, name: name);
        }

        protected override string GetModulePath() => "/channel/";
    }
}
