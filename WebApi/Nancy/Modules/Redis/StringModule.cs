﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nancy.Configuration;
using Services.Abstractions;
using Services.Abstractions.Redis;

namespace WebApi.Nancy.Modules.Redis
{
    public class StringModule : RedisModule
    {
        public StringModule(IRedisClient client, INancyEnvironment environment) 
            : base(client, environment)
        {
            RouteSet(client, Path + "set", "Set");

            RouteGet(client, Path + "get", "Get");

            RouteIncrement(client, Path + "increment", "Increment");
        }

        private void RouteIncrement(IRedisClient client, string path, string name)
        {
            Func<dynamic, object> increment = args => client.String.Increment(args.databaseId, Key);

            Post(path, increment, name: name);

            Put(path, increment, name: name);
        }

        private void RouteGet(IRedisClient client, string path, string name)
        {
            Func<dynamic, object> get = args => client.String.Get(args.databaseId, Key);

            Get(path, get, name: name);
        }

        private void RouteSet(IRedisClient client, string path, string name)
        {
            Func<dynamic, object> set = args => client.String.Set(args.databaseId, Key, Value);

            Post(path, set, name: name);

            Put(path, set, name: name);
        }

        protected override string GetModulePath() => "database/{databaseId}/string/";
    }
}
