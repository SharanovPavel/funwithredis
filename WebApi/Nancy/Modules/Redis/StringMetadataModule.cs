﻿using Nancy.Metadata.Modules;
using Nancy.Swagger;
using Swagger.ObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace WebApi.Nancy.Modules.Redis
{
    public class StringMetadataModule : MetadataModule<PathItem>
    {
        public StringMetadataModule()
        {
            var tag = "string";

            var keyParameter = new Parameter
            {
                Required = true,
                Name = "key",
                Type = "string",
                In = ParameterIn.Query
            };

            var valueParameter = new Parameter
            {
                Required = true,
                Name = "value",
                Type = "string",
                In = ParameterIn.Query
            };

            var databaseIdParameter = new Parameter
            {
                In = ParameterIn.Path,
                Required = true,
                Name = "databaseId",
                Type = "number",
                Description = "Redis database identificator."
            };

            var boolSchema = new Schema
            {
                Type = "boolean"
            };

            var stringSchema = new Schema
            {
                Type = "string"
            };

            var numberSchema = new Schema
            {
                Type = "number"
            };

            Describe["Get"] = description => description.AsSwagger(
                with => with.Operation(
                    operation => operation.Tag(tag)
                                          .Summary("Get value.")
                                          .Description("Get the value of key. If the key does not exist null is returned.")
                                          .Response(200, r => r.Schema(stringSchema).Description("Value."))
                                          .ProduceMimeType("text/plain")
                                          .Parameter(keyParameter)
                                          .Parameter(databaseIdParameter)));

            Describe["Set"] = description => description.AsSwagger(
                with => with.Operation(
                    op => op.Tag(tag)
                            .Summary("Set value.")
                            .Description("Set key to hold the string value. If key already holds a value, it is overwritten, regardless of its type.")
                            .Response(200, r => r.Schema(boolSchema).Description("Success.").Example("text/plain", "true"))
                            .ProduceMimeType("text/plain")
                            .Parameter(keyParameter)
                            .Parameter(valueParameter)
                            .Parameter(databaseIdParameter)));

            Describe["Increment"] = description => description.AsSwagger(
                with => with.Operation(
                    op => op.Tag(tag)
                    .Summary("Increment value.")
                    .Description("Increments the number stored at key by increment. If the key does not exist, it is set to 0 before performing the operation.")
                    .Response(200, r => r.Schema(numberSchema).Description("Incremented value.").Example("text/plain", 1))
                    .Parameter(keyParameter)
                    .Parameter(databaseIdParameter)));
        }
    }
}
