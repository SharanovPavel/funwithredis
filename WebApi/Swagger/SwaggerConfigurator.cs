﻿using Microsoft.Extensions.Configuration;
using Nancy.Swagger.Services;
using Swagger.ObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Swagger
{
    public static class SwaggerConfigurator
    {
        public static void Configure(IConfiguration configuration)
        {
            SwaggerMetadata metadata = new SwaggerMetadata();

            configuration.Bind(metadata);

            SwaggerMetadataProvider.SetInfo(
                metadata.Info.Title,
                metadata.Info.Version,
                metadata.Info.Description,
                metadata.Contact);
        }
    }
}
