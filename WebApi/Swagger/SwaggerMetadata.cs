﻿using Swagger.ObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Swagger
{
    public class SwaggerMetadata
    {
        public ProductInformation Info { get; set; }

        public Contact Contact { get; set; }

        public class ProductInformation
        {
            public string Title { get; set; }

            public string Version { get; set; }

            public string Description { get; set; }
        }
    }
}
